FROM alpine

RUN apk --update --upgrade add ruby tmux yarn

RUN gem install --no-document tmuxinator

CMD ["tmux"]
